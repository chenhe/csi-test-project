function corrDetect(rx)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
forw=rx.forward.reduced_rx.mag;
backw=rx.backward.reduced_rx.mag;
[mag_row mag_col]=size(forw)    %row and col of the mag matrix
[row col]=size(forw{1,1});      %row and col of the mag{1,1} matrix

for i=1:mag_col         %scan the mag
    for j=1:row
        a=corrcoef(forw{1,i}(j,:),backw{1,i}(j,:));
        if abs(a(1,2))<0.5  %set threshold = 0.5
            X=sprintf('the %d data of mag %d is uncorrelated, whose value is %f',j,i,a(1,2));
            disp(X);
        end             
    end
end
end
