function magCorr_matrix = corrMatrix(rx)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
forw=rx.forward.reduced_rx.mag;
backw=rx.backward.reduced_rx.mag;
[mag_row mag_col]=size(forw)    %row and col of the mag matrix
[row col]=size(forw{1,1});      %row and col of the mag{1,1} matrix

magCorr_matrix=cell(1,mag_col);
for i=1:mag_col     %scan the mag
    singleCorr_mag_matrix=zeros(1,row);
    
    for j=1:row
        a=corrcoef(forw{1,i}(j,:),backw{1,i}(j,:));
        singleCorr_mag_matrix(1,j)=a(1,2); 
    magCorr_matrix{1,i}=singleCorr_mag_matrix;    
    end
end
end
     
